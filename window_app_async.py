import time
import asyncio
import tkinter

number = 0


async def run_window_app():
    global root
    try:
        while True:
            root.update()
            await asyncio.sleep(0.1)
    except KeyboardInterrupt:
        print("Closing the app")


async def blocking_callback():
    print("Rendering...")
    await asyncio.sleep(2)
    print("Done!")


def button_callback():
    return asyncio.create_task(blocking_callback())


def incrementing_callback():
    global number, label_number
    number += 1
    label_number["text"] = str(number)


root = tkinter.Tk()

freeze_button = tkinter.Button(root, text='Run', command=button_callback)
freeze_button.place(relx=0.5, rely=0.2, anchor=tkinter.CENTER)

label_number = tkinter.Label(root, text=str(number))
label_number.place(relx=0.5, rely=0.5, anchor=tkinter.CENTER)

increment_button = tkinter.Button(root, text='Increment', command=incrementing_callback)
increment_button.place(relx=0.5, rely=0.8, anchor=tkinter.CENTER)

asyncio.run(run_window_app())
