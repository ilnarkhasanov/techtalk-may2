import time
import tkinter
from threading import Thread

number = 0


def blocking_callback():
    print("Rendering...")
    time.sleep(2)
    print("Done!")


def thread_callback():
    thread = Thread(target=blocking_callback)
    thread.start()


def incrementing_callback():
    global number, label_number
    number += 1
    label_number["text"] = str(number)


root = tkinter.Tk()

freeze_button = tkinter.Button(root, text='Run', command=thread_callback)
freeze_button.place(relx=0.5, rely=0.2, anchor=tkinter.CENTER)

label_number = tkinter.Label(root, text=str(number))
label_number.place(relx=0.5, rely=0.5, anchor=tkinter.CENTER)

increment_button = tkinter.Button(root, text='Increment', command=incrementing_callback)
increment_button.place(relx=0.5, rely=0.8, anchor=tkinter.CENTER)

if __name__ == "__main__":
    root.mainloop()
